//Possible answers provided on Magic 8-Ball page
const possibleAnswers = ["Outlook not so good.", "You may rely on it.", "Signs point to yes.", "Better not tell you now.","It is known.", "It will not be.", "Your questions will be affirmed.", "Not the way I see it.", "It cannot be denied.", "It is ill-fated.", "Undoubtedly.", "The situation dictates no.", "It is certain.", "Concentrate and ask again."]

//function for button
function eightBall () {
    //assign randAns as a random value from possible answers, 14 total
    var randAns = possibleAnswers[Math.floor(Math.random() * 14)];
    //create div element for answer
    var answer = document.createElement("div");
    //assign text content to be the random answer
    answer.textContent = randAns;
    var total = document.getElementById("answer");
    //append action
    total.appendChild(answer);
}

